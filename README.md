```bash
isafe3(new)
├── ASET
│   ├── GET /aset ✗
│   └── GET /aset/goldenRules ✗
├── HR
│   ├── GET HR ✓
│   ├── GET HR by id ✗ didefine tapi tidak dipakai
│   ├── POST Reassign HR ✓
│   ├── POST /api/v1/hr/submit ✓
│   ├── POST /api/v1/hr/savetodraf ✗ didefine tapi tidak dipakai
│   ├── DEL DELETE HR by id ✗ didefine tapi tidak dipakai
│   ├── GET hr.accept ✓
│   ├── POST hr.submitinv ✓
│   └── POST hr/upload-image ✓
├── INSPEKSI 
│   ├── GET /api/v1/inspeksi ✗ tidak didefine
│   ├── POST /api/v1/inspeksi/submit ✓
│   ├── POST /api/v1/inspeksi/submitNC ✓
│   ├── GET /api/v1/inspeksi/mySubmitedInspeksi ✓
│   ├── GET /api/v1/inspeksi/myTodoNC ✓
│   ├── GET /api/v1/inspeksi/myTodoNC/:nc_id/accept ✓
│   └── GET /api/v1/inspeksi/myTodoNC/:nc_id/close ✓
├── USER
│   ├── GET All User ✗ didefine tapi tidak dipakai
│   ├── POST user by id ✓ untuk ngambil profile pertama kali
│   └── GET All Simper List ◦
├── LOCATION
│   └── GET /findMasterLocation ✗ tidak dipakai diganti dengan fungsi berbeda di sync
├── NC
│   ├── GET penyebabLangsung ✗ sudah digantikan di sync
│   └── GET penyebabDasar ✗ sudah digantikan di sync
├── CUTI
│   ├── POST submit ✓ dipanggil di profile
│   ├── GET mySubmittedCuti ✓ dipanggil di profile
│   ├── GET myAssignedCuti ✓ dipanggil di profile
│   ├── GET reject ✓ dipanggil di home
│   ├── GET accept ✓ dipanggil di home
│   ├── GET cutOff ✗ tidak ada dicode dan tidak diimplement
│   ├── GET checkBeforeCuti ✓ dipanggil di home 
│   ├── GET chekAfterCuti ✗ tidak ada dicode dan tidak diimplement
│   ├── GET chekOnCuti ✗ tidak ada dicode dan tidak diimplement
│   ├── GET checkLastCuti ✓ dipanggil di home
│   ├── GET myAcceptOrRejectedCuti ✓ dipanggil di home
│   └── GET readNotif ✓ dipanggil di home
├── MENTARI
│   └── GET / ✓ dipanggil di profile
├── OBSERVASI
│   ├── GET /api/v1/observasi/ ✗ tidak ada dicode dan tidak diimplement
│   ├── GET /api/v1/observasi/myTodoNC ✓ dipanggil di mytodonc dan dimasukkan ke rxdb
│   ├── GET /api/v1/observasi/:assesment_id/seen ✓ dipanggil di ditail observasi
│   ├── GET /api/v1/observasi/myTodoNC/:id ✗ tidak ada dicode dan tidak diimplement
│   ├── GET /api/v1/observasi/myTodoNC/:nc_id/accept ✓ dipanggil accept nc observasi
│   ├── GET /api/v1/observasi/myTodoNC/:nc_id/close ✓ dipanggil close nc observasi
│   ├── POST /api/v1/observasi/submit ✓ untuk submit observasi
│   └── POST /api/v1/observasi/submitNC ✓ untuk submit tindaklanjut observasi
├── SYNC
│   ├── GET /sync/dsis_master_location ✓
│   ├── GET /sync/dsis_master_sub_lokasi ✓
│   ├── GET /sync/dsis_master_ketidaksesuaian ✓
│   ├── GET /sync/dsis_master_analisa_penyebab_langsung ✓
│   ├── GET /sync/dsis_master_penyebab_dasar ✓
│   ├── GET /sync/dsis_hse_simper_competency ✓
│   ├── GET /sync/dsis_hse_simper_training ✓
│   ├── GET /sync/dsis_hse_simper_family ✓
│   ├── GET /sync/dsis_hse_simper_pelanggaran ✓
│   ├── GET /sync/dsis_hse_simper ✓
│   ├── GET /sync/dsis_master_tipe_aset ✓
│   ├── GET /sync/dsis_master_tipe_unit ✓
│   ├── GET /sync/dsis_master_form ✓
│   ├── GET /sync/dsis_master_form_questions ✓
│   ├── GET /sync/dsis_master_form_questions_group ✓
│   ├── GET /sync/dsis_master_unit ✓
│   ├── GET /sync/dsis_master_unit_prasarana ✓
│   ├── GET /sync/dsis_master_unit_peralatan ✓
│   ├── GET /sync/dsis_master_unit_instalasi ✓
│   ├── GET /sync/dsis_master_kontraktor ✓
│   ├── GET /sync/dsis_master_department ✓
│   ├── GET /sync/dsis_master_position ✓
│   └── {{local}}/api/v1/aset ✗
├── login
│   ├── POST Login ✗ sudah tidak ada di code
│   ├── POST Change Password ✓
│   ├── POST Forgot Password ✓
│   ├── POST Login Force Local ◦
│   └── POST Login Force ✓
├── Logout
│   └── POST logout ✓
├── Campaign
│   └── Get campaign ✓
├── SIMPER ◦
├── SUBLOKASI ◦
└── GET ✗ sudah ada mungkin salah copas
```